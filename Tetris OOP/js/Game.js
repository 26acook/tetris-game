import {Block} from "./Block.js";

class Game {
    #arenaHeight;
    #arenaWidth;
    #dropCounter = 0;
    #dropInterval = 1000;
    #lastTime = 0;
    arena;
    activeBlock;

    constructor({canvasElement, arenaHeight, arenaWidth}) {

        if (!canvasElement) {
            throw new Error("Canvas element not found");
        }

        if (!arenaHeight || !arenaWidth) {
            throw new Error("Arena dimensions are not provided");
        }

        this.#arenaHeight = arenaHeight;
        this.#arenaWidth = arenaWidth;
        this.canvasElement = canvasElement;
        this.context = canvasElement.getContext('2d');
        this.activeBlock = new Block();
        this.arena = Game.createMatrix(this.#arenaWidth, this.#arenaHeight);
    }

    #drawMatrix(matrix, offset) {
        matrix.forEach((row, y) => {
            row.forEach((value, x) => {
                if (value !== 0) {
                    this.context.fillStyle = this.activeBlock.colors[value];
                    this.context.fillRect(x + offset.x, y + offset.y, 1, 1);
                }
            });
        });
    }

    #arenaSweep() {
        outer: for (let y = this.arena.length -1; y > 0; --y) {
            for (let x = 0; x < this.arena[y].length; ++x) {
                if (this.arena[y][x] === 0) {
                    continue outer;
                }
            }

            const row = this.arena.splice(y, 1)[0].fill(0);
            this.arena.unshift(row);
            ++y;
        }
    }

    #draw() {
        this.context.fillStyle = '#181515';
        this.context.fillRect(0, 0, this.canvasElement.width, this.canvasElement.height);
        this.#drawMatrix(this.arena ,{x: 0, y: 0});
        this.#drawMatrix(this.activeBlock.shape, this.activeBlock.coordinates);
    }

    #mergeBlock() {
        this.activeBlock.shape.forEach((row, y) => {
            row.forEach((value, x) => {
                if(value !== 0) {
                    this.arena[y + this.activeBlock.coordinates.y][x + this.activeBlock.coordinates.x] = value;
                }
            })
        });
    }

    #update(time = 0) {
        const deltaTime = time - this.#lastTime;
        this.#lastTime = time;

        this.#dropCounter += deltaTime;
        if (this.#dropCounter > this.#dropInterval) {
            this.blockDrop();
        }

        this.#draw(time);
        requestAnimationFrame(this.#update.bind(this));
    }

    blockDrop() {
        this.activeBlock.moveDown();
        this.#dropCounter = 0;
        if(this.activeBlock.collide(this.arena)) {
            this.activeBlock.moveUp();
            this.#mergeBlock();
            this.activeBlock.createRandomBlock();
            this.activeBlock.coordinates.y = 0;
            this.activeBlock.coordinates.x = Math.floor((this.arena[0].length / 2) - (this.activeBlock.shape[0].length / 2));
            if(this.activeBlock.collide(this.arena)) {
                this.arena.forEach(row => row.fill(0));
            }
            this.#arenaSweep();
        }
    }

    start() {
        this.context.scale(20, 20);
        this.#update();
    }

    static createMatrix(w, h) {
        const matrix = [];
        while (h--) {
            matrix.push(new Array(w).fill(0));
        }
        return matrix;
    }

}

const game = new Game({
    canvasElement: document.getElementById('arena'),
    arenaWidth: 12,
    arenaHeight: 20,
});

game.start();

document.addEventListener('keydown', event => {
    if (event.keyCode === 37) {
        game.activeBlock.moveLeft();
        if(game.activeBlock.collide(game.arena)) {
            game.activeBlock.moveRight();
        }
    } else if (event.keyCode === 39) {
        game.activeBlock.moveRight();
        if(game.activeBlock.collide(game.arena)) {
            game.activeBlock.moveLeft();
        }
    } else if (event.keyCode === 40) {
        game.blockDrop();
    } else if (event.keyCode === 81) {
        game.activeBlock.rotateBlock(-1, game.arena);
    } else if (event.keyCode === 87) {
        game.activeBlock.rotateBlock(1, game.arena);
    }
})
